package ru.ulstu.is.sbapp.companyTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.service.DepartmentService;
import ru.ulstu.is.sbapp.company.service.EmployeeService;
import ru.ulstu.is.sbapp.company.service.PositionService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
public class EmployeeTests {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private PositionService positionService;

    @Test
    void testEmployeeCreate() {
        employeeService.deleteAllEmployees();
        final Employee employee = employeeService.addEmployee("Иван", "Иванов");
        Assertions.assertNotNull(employee.getId());
    }

    @Test
    void testEmployeeRead() {
        employeeService.deleteAllEmployees();
        final Employee employee = employeeService.addEmployee("Иван", "Иванов");
        final Employee findEmployee = employeeService.findEmployee(employee.getId());
        Assertions.assertEquals(employee, findEmployee);
    }

    @Test
    void testEmployeeReadNotFound() {
        employeeService.deleteAllEmployees();
        Assertions.assertThrows(EntityNotFoundException.class, () -> employeeService.findEmployee(-1L));
    }

    @Test
    void testEmployeeReadAll() {
        employeeService.deleteAllEmployees();
        employeeService.addEmployee("Иван", "Иванов");
        employeeService.addEmployee("Петр", "Петров");
        final List<Employee> employees = employeeService.findAllEmployees();
        Assertions.assertEquals(employees.size(), 2);
    }

    @Test
    void testEmployeeReadAllEmpty() {
        employeeService.deleteAllEmployees();
        final List<Employee> employees = employeeService.findAllEmployees();
        Assertions.assertEquals(employees.size(), 0);
    }

    @Test
    void testAddingDepartmentToEmployee() {
        departmentService.deleteAllDepartments();
        employeeService.deleteAllEmployees();
        Department department = departmentService.addDepartment("Отдел 1");
        Employee employee = employeeService.addEmployee("Кирилл", "Тепечин");

        employeeService.addEmployeeDepartment(employee.getId(), department.getId());

        department = departmentService.findDepartment(department.getId());
        employee = employeeService.findEmployee(employee.getId());

        Assertions.assertEquals(employee.getDepartments().get(0), department);
    }

    @Test
    void testAddingPositionToEmployee() {
        employeeService.deleteAllEmployees();
        positionService.deleteAllPositions();
        Position position = positionService.addPosition("Руководитель", 100000);
        Employee employee = employeeService.addEmployee("Кирилл", "Тепечин");

        employeeService.addPosition(employee.getId(), position.getId());

        position = positionService.findPosition(position.getId());
        employee = employeeService.findEmployee(employee.getId());

        Assertions.assertEquals(employee.getPosition(), position);
    }
}
