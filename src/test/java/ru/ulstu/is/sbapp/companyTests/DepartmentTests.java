package ru.ulstu.is.sbapp.companyTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.service.DepartmentService;
import ru.ulstu.is.sbapp.company.service.EmployeeService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
public class DepartmentTests {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private EmployeeService employeeService;
    @Test
    void testDepartmentCreate() {
        departmentService.deleteAllDepartments();
        final Department department = departmentService.addDepartment("Отдел1");
        Assertions.assertNotNull(department.getId());
    }

    @Test
    void testDepartmentRead() {
        departmentService.deleteAllDepartments();
        final Department department = departmentService.addDepartment("Отдел1");
        final Department findDep = departmentService.findDepartment(department.getId());
        Assertions.assertEquals(department, findDep);
    }

    @Test
    void testDepartmentReadNotFound() {
        departmentService.deleteAllDepartments();
        Assertions.assertThrows(EntityNotFoundException.class, () -> departmentService.findDepartment(-1L));
    }

    @Test
    void testDepartmentReadAll() {
        departmentService.deleteAllDepartments();
        departmentService.addDepartment("Отдел1");
        departmentService.addDepartment("Отдел2");
        final List<Department> departments = departmentService.findAllDepartments();
        Assertions.assertEquals(departments.size(), 2);
    }

    @Test
    void testDepartmentReadAllEmpty() {
        departmentService.deleteAllDepartments();
        final List<Department> departments = departmentService.findAllDepartments();
        Assertions.assertEquals(departments.size(), 0);
    }

    @Test
    void testAddingEmployeeToDepartment() {
        departmentService.deleteAllDepartments();
        employeeService.deleteAllEmployees();
        Department department = departmentService.addDepartment("Отдел 1");
        Employee employee = employeeService.addEmployee("Кирилл", "Тепечин");

        departmentService.addDepartmentEmployee(department.getId(), employee.getId());

        department = departmentService.findDepartment(department.getId());
        employee = employeeService.findEmployee(employee.getId());

        Assertions.assertEquals(employee.getDepartments().get(0), department);
    }

}
