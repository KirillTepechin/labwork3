package ru.ulstu.is.sbapp.companyTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.service.EmployeeService;
import ru.ulstu.is.sbapp.company.service.PositionService;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@SpringBootTest
public class PositionTests {

    @Autowired
    private PositionService positionService;
    @Autowired
    private EmployeeService employeeService;

    @Test
    void testPositionCreate() {
        positionService.deleteAllPositions();
        final Position position = positionService.addPosition("Руководитель", 70000);
        Assertions.assertNotNull(position.getId());
    }

    @Test
    void testPositionRead() {
        positionService.deleteAllPositions();
        final Position position = positionService.addPosition("Руководитель", 70000);
        final Position findPosition = positionService.findPosition(position.getId());
        Assertions.assertEquals(position, findPosition);
    }

    @Test
    void testPositionReadNotFound() {
        positionService.deleteAllPositions();
        Assertions.assertThrows(EntityNotFoundException.class, () -> positionService.findPosition(-1L));
    }

    @Test
    void testPositionReadAll() {
        positionService.deleteAllPositions();
        positionService.addPosition("Руководитель", 70000);
        positionService.addPosition("Бухгалтер", 50000);
        final List<Position> positions = positionService.findAllPositions();
        Assertions.assertEquals(positions.size(), 2);
    }

    @Test
    void testPositionReadAllEmpty() {
        positionService.deleteAllPositions();
        final List<Position> positions = positionService.findAllPositions();
        Assertions.assertEquals(positions.size(), 0);
    }

    @Test
    void testAddingEmployeeToPosition() {
        positionService.deleteAllPositions();
        employeeService.deleteAllEmployees();
        Position position = positionService.addPosition("Руководитель", 100000);
        Employee employee = employeeService.addEmployee("Кирилл", "Тепечин");

        positionService.addPositionEmployee(position.getId(), employee.getId());

        position = positionService.findPosition(position.getId());
        employee = employeeService.findEmployee(employee.getId());

        Assertions.assertEquals(position.getEmployees().get(0), employee);
    }
}
