package ru.ulstu.is.sbapp.company.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.service.EmployeeService;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/{id}")
    public Employee getEmployee(@PathVariable Long id) {
        return employeeService.findEmployee(id);
    }

    @GetMapping("/")
    public List<Employee> getEmployees() {
        return employeeService.findAllEmployees();
    }

    @PostMapping("/")
    public Employee createEmployee(@RequestParam("firstName") String firstName,
                                 @RequestParam("lastName") String lastName) {
        return employeeService.addEmployee(firstName, lastName);
    }

    @PatchMapping("/{id}")
    public Employee updateEmployee(@PathVariable Long id,
                                 @RequestParam("firstName") String firstName,
                                 @RequestParam("lastName") String lastName) {
        return employeeService.updateEmployee(id, firstName, lastName);
    }

    @DeleteMapping("/{id}")
    public Employee deleteEmployee(@PathVariable Long id) {
        return employeeService.deleteEmployee(id);
    }
}
