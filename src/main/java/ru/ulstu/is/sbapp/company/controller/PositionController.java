package ru.ulstu.is.sbapp.company.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.service.EmployeeService;
import ru.ulstu.is.sbapp.company.service.PositionService;

import java.util.List;

@RestController
@RequestMapping("/position")
public class PositionController {

    private final PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @GetMapping("/{id}")
    public Position getPosition(@PathVariable Long id) {
        return positionService.findPosition(id);
    }

    @GetMapping("/")
    public List<Position> getPositions() {
        return positionService.findAllPositions();
    }

    @PostMapping("/")
    public Position createPosition(@RequestParam("name") String name,
                                   @RequestParam("salary") int salary) {
        return positionService.addPosition(name, salary);
    }

    @PatchMapping("/{id}")
    public Position updatePosition(@PathVariable Long id,
                                   @RequestParam("name") String name,
                                   @RequestParam("salary") int salary) {
        return positionService.updatePosition(id, name, salary);
    }

    @DeleteMapping("/{id}")
    public Position deletePosition(@PathVariable Long id) {
        return positionService.deletePosition(id);
    }
}
