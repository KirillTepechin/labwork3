package ru.ulstu.is.sbapp.company.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.service.DepartmentService;

import java.util.List;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/{id}")
    public Department getDepartment(@PathVariable Long id) {
        return departmentService.findDepartment(id);
    }

    @GetMapping("/")
    public List<Department> getDepartments() {
        return departmentService.findAllDepartments();
    }

    @PostMapping("/")
    public Department createDepartment(@RequestParam("name") String name) {
        return departmentService.addDepartment(name);
    }

    @PatchMapping("/{id}")
    public Department updateDepartment(@PathVariable Long id,
                                       @RequestParam("name") String name) {
        return departmentService.updateDepartment(id, name);
    }

    @DeleteMapping("/{id}")
    public Department deleteDepartment(@PathVariable Long id) {
        return departmentService.deleteDepartment(id);
    }
}
