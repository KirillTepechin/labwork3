package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class DepartmentService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Department addDepartment(String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("Department name is null or empty");
        }
        final Department department = new Department(name);
        em.persist(department);
        return department;
    }

    @Transactional(readOnly = true)
    public Department findDepartment(Long id) {
        final Department department = em.find(Department.class, id);
        if (department == null) {
            throw new EntityNotFoundException(String.format("Department with id [%s] is not found", id));
        }
        return department;
    }

    @Transactional(readOnly = true)
    public List<Department> findAllDepartments() {
        return em.createQuery("select d from Department d", Department.class)
                .getResultList();
    }

    @Transactional
    public Department updateDepartment(Long id, String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("Department name is null or empty");
        }
        final Department currentDepartment = findDepartment(id);
        currentDepartment.setName(name);
        return em.merge(currentDepartment);
    }

    @Transactional
    public Department deleteDepartment(Long id) {
        final Department currentDepartment = findDepartment(id);
        em.remove(currentDepartment);
        return currentDepartment;
    }

    @Transactional
    public void deleteAllDepartments() {
        em.createQuery("delete from Department").executeUpdate();
    }

    @Transactional
    public void addDepartmentEmployee(Long departmentId, Long employeeId) {
        var department = findDepartment(departmentId);
        var employee = em.find(Employee.class, employeeId);
        department.addEmployee(employee);
        em.merge(employee);
    }
}
