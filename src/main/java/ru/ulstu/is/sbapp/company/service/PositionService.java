package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class PositionService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Position addPosition(String name, int salary) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("Position name is null or empty");
        }
        final Position position = new Position(name, salary);
        em.persist(position);
        return position;
    }

    @Transactional(readOnly = true)
    public Position findPosition(Long id) {
        final Position position = em.find(Position.class, id);
        if (position == null) {
            throw new EntityNotFoundException(String.format("Position with id [%s] is not found", id));
        }
        return position;
    }

    @Transactional(readOnly = true)
    public List<Position> findAllPositions() {
        return em.createQuery("select p from Position p", Position.class)
                .getResultList();
    }

    @Transactional
    public Position updatePosition(Long id, String name, int salary) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("Position name is null or empty");
        }
        final Position currentPosition = findPosition(id);
        currentPosition.setName(name);
        currentPosition.setSalary(salary);
        return em.merge(currentPosition);
    }

    @Transactional
    public Position deletePosition(Long id) {
        final Position currentPosition = findPosition(id);
        em.remove(currentPosition);
        return currentPosition;
    }

    @Transactional
    public void deleteAllPositions() {
        em.createQuery("delete from Position").executeUpdate();
    }

    @Transactional
    public Employee addPositionEmployee(Long positionId, Long employeeId) {
        var employee = em.find(Employee.class, employeeId);
        var position = findPosition(positionId);
        position.addEmployee(employee);
        return em.merge(employee);
    }
}
