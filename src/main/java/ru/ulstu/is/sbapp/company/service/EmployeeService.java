package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class EmployeeService {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Employee addEmployee(String firstName, String lastName) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName)) {
            throw new IllegalArgumentException("Employee name is null or empty");
        }
        final Employee employee = new Employee(firstName, lastName);
        em.persist(employee);
        return employee;
    }

    @Transactional(readOnly = true)
    public Employee findEmployee(Long id) {
        final Employee employee = em.find(Employee.class, id);
        if (employee == null) {
            throw new EntityNotFoundException(String.format("Employee with id [%s] is not found", id));
        }
        return employee;
    }

    @Transactional(readOnly = true)
    public List<Employee> findAllEmployees() {
        return em.createQuery("select e from Employee e", Employee.class)
                .getResultList();
    }

    @Transactional
    public Employee updateEmployee(Long id, String firstName, String lastName) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName)) {
            throw new IllegalArgumentException("Student name is null or empty");
        }
        final Employee currentEmployee = findEmployee(id);
        currentEmployee.setFirstName(firstName);
        currentEmployee.setLastName(lastName);
        return em.merge(currentEmployee);
    }

    @Transactional
    public Employee deleteEmployee(Long id) {
        final Employee currentEmployee = findEmployee(id);
        em.remove(currentEmployee);
        return currentEmployee;
    }

    @Transactional
    public void deleteAllEmployees() {
        em.createQuery("delete from Employee").executeUpdate();
    }

    @Transactional
    public void addEmployeeDepartment(Long employeeId, Long departmentId) {
        var employee = findEmployee(employeeId);
        var department = em.find(Department.class, departmentId);
        employee.addDepartment(department);
        em.merge(employee);
    }

    @Transactional
    public Employee addPosition(Long employeeId, Long positionId) {
        var employee = findEmployee(employeeId);
        var position = em.find(Position.class, positionId);
        employee.setPosition(position);
        return em.merge(employee);
    }

}
